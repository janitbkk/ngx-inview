import { Injectable, Inject } from '@angular/core';
import { WINDOW } from '@casdl/ngx-window';

@Injectable()
export class WindowRuler {
  private _windowRect: ClientRect;

  constructor(
    @Inject(WINDOW) private window
  ) {
    this._windowRect = this._createWindowRect();
  }

  onChange() {
    this._windowRect = this._createWindowRect();
  }

  getWindowViewPortRuler() {
    return this._windowRect;
  }

  private _createWindowRect() {
    const height = this.window.innerHeight;
    const width = this.window.innerWidth;
    return {
      top: 0,
      left: 0,
      bottom: height,
      right: width,
      height,
      width
    };
  }
}












