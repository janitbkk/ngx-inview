import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { WINDOW } from '@casdl/ngx-window';

import { Observable, fromEvent, of, merge } from 'rxjs';
import { share, tap } from 'rxjs/operators';

import { WindowRuler } from './viewport-ruler';
import { WindowElement } from './models';

// @dynamic
@Injectable()
export class ScrollObservable {
  static _globalObservable: Observable<any>;
  static _elementObservableReferences: Map<WindowElement, Observable<any>> = new Map();
  static isWindow(windowElement: WindowElement) {
    return Object.prototype.toString.call(windowElement).includes('Window');
  }

  get isBrowser(): boolean {
    return isPlatformBrowser(this.platformId);
  }

  constructor(
    @Inject(WINDOW) private window,
    @Inject(PLATFORM_ID) private platformId: Object,
    private _windowRuler: WindowRuler
  ) {
    if (!ScrollObservable._globalObservable) {
      ScrollObservable._globalObservable = this._getGlobalObservable();
    }
  }
  private _getGlobalObservable(): Observable<any> {
    const observable = this.isBrowser 
    ? [ fromEvent(this.window.document, 'scroll'), 
        fromEvent(this.window, 'resize').pipe(
          tap((event: any) => this._windowRuler.onChange())
        )
      ]
    : [of()];

    return merge(...observable).pipe(share());
  }
  scrollObservableFor(windowElement: WindowElement): Observable<any> {
    if (ScrollObservable.isWindow(windowElement)) {
      return ScrollObservable._globalObservable;
    }
    if (ScrollObservable._elementObservableReferences.has(windowElement)) {
      return <Observable<any>>ScrollObservable._elementObservableReferences.get(windowElement);
    }
    const ref = this._createElementObservable(windowElement);
    ScrollObservable._elementObservableReferences.set(windowElement, ref);
    return ref;
  }
  private _createElementObservable(windowElement: WindowElement): Observable<any> {
    return this.isBrowser 
      ? fromEvent(windowElement, 'scroll').pipe(share())
      : of();
  }

}
