import { NgModule, ModuleWithProviders } from '@angular/core';

import { InviewDirective } from './inview.directive';
import { InviewContainerDirective } from './inview-container.directive';
import { InviewItemDirective } from './inview-item.directive';
import { ScrollObservable } from './utils/scroll-observable';
import { WindowRuler } from './utils/viewport-ruler';

export * from './inview.directive';
export * from './inview-container.directive';
export * from './inview-item.directive';
export * from './utils/scroll-observable';
export * from './utils/viewport-ruler';

@NgModule({
  declarations: [InviewDirective, InviewContainerDirective, InviewItemDirective],
  exports: [InviewDirective, InviewContainerDirective, InviewItemDirective]
})
export class InViewModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: InViewModule,
      providers: [ScrollObservable, WindowRuler]
    }
  }
}
