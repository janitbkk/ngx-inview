/**
 * This is only for local test
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { InViewModule }  from '../dist';

@Component({
  selector: 'app',
  template: `<div in-view-container (inview)="tester($event)">
    <div style="height:1000px; width: 100%; background: red;" in-view-item id="aa">aa</div>
    <div style="height:1000px; width: 100%; background: blue;" in-view-item id="bb">bb</div>
  </div>`
})
class AppComponent {
  tester(event: any) {
    console.log(event);
  }
}

@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [ AppComponent ],
  imports: [ BrowserModule, InViewModule.forRoot() ]
})
class AppModule {}

platformBrowserDynamic().bootstrapModule(AppModule);
